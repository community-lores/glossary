# Hey Vsauce, ~~Michael~~ Gildedguy Here!

_...or you mean, how to not be confused between an Michael Stevens from an Michael Moy?_

## Why this page ever created?

It's actually started as an meme from [Gildedguy's (Michael Moy)](../../lexicons/hyundojo/mikedmoy.md)

This should served as an document for confused Internet users between these two Michaels here (atleast in this case, Vsauce and Gildedguy the animator here, before we start adding more Michaels here)

## How not to get confused?

TL;DR: Use the content creator's public name as much as possible.

## Share This and contributions

Canonical URLs for this `git diff` article are:

* `https://glossary.community-lores.tk/git-diff/hey-vsauce-gildedguy-here` - most services block those free domains from Freemon, so you probably use the shortlink below instead
* `https://kutt.it/hey-vsauce-gildedguy-here` - much better, some sites might don't like shortlinks at all
* `https://gitlab.com/Community-Lores/glossary/blob/main/docs/concepts/git-diff/hey-vsauce-gildedguy-here.md` - a lot longer, just in case they're sussing out `kutt.it` links and also optionally replace `gitlab.com` with `github.com` if they also prefer GitHub (at least some GitLab-only markup might break like footnotes)

### Contributions

This article is made and currently maintained by [Andrei Jiroh himself](https://gitlab.com/ajhalili2006).
Add your name here to make your contributions count and also to help reusers and remixed to credit everybody.

---

Content available freely licensed under CC BY-SA 4.0 International. When you using some text here or even remixing/forking this, please provide attribution to this article, [as per this guideline](..).
