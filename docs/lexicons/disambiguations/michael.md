# Why there are so many Michaels here...

_...perhaps in this universe right? (unless we ever `git merge` them all in an simulated world)_

## Some noteworthy examples

* 
* The Wikipedia disambiguation page for this common name, in case we cannot cover them all

## Related articles

Help needed in this section by providing links related to this topic. [Submit them through an merge request.](../../../CONTRIBUTING.md).
