# Copyright License for Maintainers

This content has been moved to [`../../legalese/copyright-license.md`](../../legalese/copyright-license.md), and the maintainers has decided to park this page to avoid possible link breakage.
