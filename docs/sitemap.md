# Sitemap

This is an living document containing every single article ever in Community Lores Glossary. Both the maintainers and contributors should keep this page up-to-date as possible as they add more content or move something to its new URL while keeping the old one to avoid breaking anything.

## Concepts

### `git diff` Section

We disucss about the difference between stuff, one `git diff` at an time. Sometimes, we didn't use `git diff` at all in writing of these articles.

* [Hey Vsauce, Gildedguy Here](concepts/git-diff/hey-vsauce-gildedguy-here.md), atleast Vsauce is Michael Moy, somewhere in an alternative universe.

### Internal Stuff at The Pins Team/Recap Time

Sometimes, we maintain an lexicon of words we used internally, especially in handbooks and other documentation.

_Nothing to see here, so far._

## Lexicons

Vocabularies used by specific group of people, or in specific contexts.

### Word-based Disambiguation

Sometimes, an word is being used across different contexts, especially some names commonly used.

* [Common Name - Michael](lexicons/disambiguations/michael.md): God please, why there are so many Michaels here?!
