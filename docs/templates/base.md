# Base Template File

## How not to get confused?

TL;DR: Use the content creator's public name as much as possible. We don't need legalese BS here.

## Share This and contributions

Canonical URLs for this article that you can use for sharing are are:

* `https://glossary.community-lores.tk/templates/base` - most services block those free domains from Freemon, so you probably use the shortlink below instead
* `https://kutt.it/update-this-url` - much better, some sites might don't like shortlinks at all
* `https://gitlab.com/Community-Lores/glossary/blob/main/docs/category/subcategory/topic-slug-here.md` - a lot longer, just in case they're sussing out `kutt.it` links and also optionally replace `gitlab.com` with `github.com` if they also prefer GitHub (at least some GitLab-only markup might break like footnotes)

### Contributions

This article is made and currently maintained by [your name here themselves](https://gitlab.com/ajhalili2006).
Add your name here to make your contributions count and also to help reusers and remixed to credit everybody.

---

Content available freely licensed under CC BY-SA 4.0 International. When you using some text here or even remixing/forking this, please provide attribution to this article, [as per this guideline](../legalese/copyright-license.md), under the `For content reusers` section. Other content such as embedded media and content from others aren't covered by this content license and your mileage may vary.
